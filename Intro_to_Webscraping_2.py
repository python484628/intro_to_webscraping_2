# Author : Marion Estoup 
# E-mail : marion_110@hotmail.fr
# November 2021




##################################################### Web crawling 


# We use only authorized URLs

# Import library 
import os

# Use --ssl-no-revoke if you are with windows, otherwise it shouldn't be necessary
result = os.popen("curl --ssl-no-revoke https://www.facebook.com/robots.txt").read()

# For loop
for line in result.split("\n"):
    if line.startswith('Allow'):
        print('Allow'+line.split(': ')[1].split(' ')[0])
    elif line.startswith('Disallow'):
        print("Disallow "+line.split(': ')[1].split(' ')[0])




# If there isn't any user agent, we need a query
# Import library
import requests
# Define link
url = 'https://httpbin.org/user-agent'

# If we copy and paste the above link in our browser (chrome, safari, ...) we can have the following : 
# {
#  "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"
# }


# If we try to crawl with python it's possible to be rejected
# But to "fake" the identity of a software we can create the query header like this :
user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'
headers = {'User-Agent': user_agent}  

response = requests.get(url, headers=headers)

html = response.content
print(response.content)
# b'{\n  "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"\n}\n'


# Website whatismybrowser.com 
# We can use user agent of apple, redmi, chrome, ...
# We have to take the one that corresponds to our search, what we want to crawl






# Another method with xpath
# More efficient
import requests
from parsel import Selector
response = requests.get('http://www.google.fr/')
selector = Selector(response.text)
href_links = selector.xpath('//a/@href').getall()
image_links=selector.xpath('//img/@src').getall()
print(href_links)
print(image_links)






# Another method with HTML parser
from html.parser import HTMLParser
import requests
html_string = requests.get('http://www.facebook.fr/')
html_string=html_string.text

class Parser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.in_a=[]
        
    def handle_starttag(self,tag,attrs):
        if(tag=='a'):
            attrs=dict(attrs)
            if ('href' in attrs):
                self.in_a.append(attrs["href"])
                
    def handle_endtag(self,tag):
        if(tag=='a'):
            self.in_a.pop()
            
    def handle_data(self,data):
        if self.in_a:
            print("<a> link :", self.in_a)
            
parser = Parser()
parser.feed(html_string)
        
        
        